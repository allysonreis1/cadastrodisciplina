package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import CadastroDisciplinas.Disciplina;

public class DisciplinaTest {

	Disciplina umaDisciplina;
	
	@Before
	public void iniciandoObjetos () {
		umaDisciplina = new Disciplina("OO", "222222");
	}
	
	@Test
	public void getDisciplinatest() {
		assertEquals("OO", umaDisciplina.getDisciplina());
	}
	
	@Test
	public void getCodigo () {
		assertEquals("222222", umaDisciplina.getCodigo());
	}

}
