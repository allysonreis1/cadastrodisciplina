package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import CadastroDisciplinas.Aluno;

public class AlunoTest {

	Aluno umAluno;
	
	@Before
	public void iniciandoObjetos () {
		umAluno = new Aluno("Junior","11/1111111");
	}
	
	@Test
	public void getNometest() {
		assertEquals("Junior", umAluno.getNome());
		
		
	}
	
	@Test 
	 public void getMatricula(){
		assertEquals("11/1111111", umAluno.getMatricula());
		
	}
	
	
}
